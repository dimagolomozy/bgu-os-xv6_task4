// Create a zombie process that
// must be reparented at exit.

#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int
main(void) {
    printf(1, "Started user test!\n");
    int fd1,fd2,fd3,fd4;
    fd1 = open("File1.txt", O_CREATE);
    fd2 = open("File2.txt", O_CREATE);
    fd3 = open("File3.txt", O_CREATE);
    fd4 = open("File4.txt", O_CREATE);

    close(fd2);

    chdir("hi");
    sleep(100000);
    exit();
}
