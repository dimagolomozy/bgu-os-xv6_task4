#include "types.h"
#include "stat.h"
#include "defs.h"
#include "param.h"
#include "traps.h"
#include "spinlock.h"
#include "fs.h"
#include "file.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "x86.h"

#define PROCFS_DIR 		0
#define PROCFS_PID		1
#define PROCFS_FILE 	2
#define PROCFS_FD		3
#define INUM_START		200

const char *fileType[3] = { "FD_NONE", "FD_PIPE", "FD_INODE" };
const char *procstateNames[6] = { "Unused", "Embryo", "Sleeping","Runnable","Running","Zombie" };
const char *pidLevelsNames[5] = { "cmdline", "cwd", "exe", "fdinfo", "status" };
enum pidLevels { CMDLINE, CWD, EXE, FDINFO, STATUS };

extern struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

char* itoa(int val)
{
	if (val == 0)
		return "0";

	static char buf[32] = {0};
	int i = 30;
	for(; val && i ; --i, val /= 10)
		buf[i] = "0123456789abcdef"[val % 10];
	return &buf[i+1];
}

int 
procfsisdir(struct inode *ip)
{
	int ret = 0;

	if (ip->minor == PROCFS_DIR || ip->minor == PROCFS_PID || ip->minor == PROCFS_FILE)
	{
		ret = 1;
	}

	return ret;
}

void 
procfsiread(struct inode* dp, struct inode *ip)
{

	ip->major = PROCFS;
	ip->size = 0;
	switch (dp->minor)
	{
	case PROCFS_DIR:
			ip->minor = PROCFS_PID;
		break;
	case PROCFS_PID:
			ip->minor = PROCFS_FILE;
		break;
	case PROCFS_FILE:
			ip->minor = PROCFS_FD;
		break;
	default:
			ip->minor = dp->minor;
		break;
	}

	ip->type = T_DEV;
	ip->flags |= I_VALID;

}

uint
printProcLevel(char *buf)
{
	struct dirent dirent;
	struct proc *p;
	int i, counter;
	char *strPid;

	acquire(&ptable.lock);

	counter = 0;
	for (i = 0; i < NPROC; i++)
	{
		p = &ptable.proc[i];
		if(p->state != UNUSED && p->state != ZOMBIE)
		{
			dirent.inum = INUM_START + i;
			strPid = itoa(p->pid);
			safestrcpy(dirent.name, strPid, strlen(strPid) + 1);
			memmove(buf + counter*sizeof(dirent), (void*)&dirent, sizeof(dirent));
			counter++;
		}
	}
	release(&ptable.lock);

	return counter*sizeof(dirent);
}

uint
printPidLevel(char * buf, uint inum)
{
	struct dirent dirent;
	struct inode *ip;
	int i;

	for (i = 0; i < sizeof(pidLevelsNames)/sizeof(char*); i++)
	{
		switch (i)
		{
			case CWD:
					dirent.inum = ptable.proc[inum - INUM_START].cwd->inum;
				break;
			case EXE:
					ip = namei(ptable.proc[inum - INUM_START].name);
					ilock(ip);
					dirent.inum = ip->inum;
					iunlock(ip);
				break;
			default:
					// div10 is proc number, mod10 is action chosen
					dirent.inum = (inum * 10) + i;
				break;
		}

		memmove(dirent.name, pidLevelsNames[i], strlen(pidLevelsNames[i]) +1);
		memmove(buf + i*sizeof(dirent), (void*)&dirent, sizeof(dirent));
	}
	return (i)*sizeof(dirent);
}

uint
printFileLevel(char *buf, uint inum)
{
	struct dirent dirent;
	struct proc *p;
	int fd, offset, procIndex, actionId;
	char *strFd;

	procIndex = ((int)(inum / 10)) - INUM_START;
	actionId = inum % 10;

	acquire(&ptable.lock);
	p = &ptable.proc[procIndex];

	offset = 0;
	switch (actionId)
	{
		case CMDLINE:
				cprintf("%s %s\n", p->cmdline, p->cmdlineArgs);
			break;
		case CWD:
				// will never get here
			break;
		case EXE:
				// will never get here
			break;
		case FDINFO:
				for (fd = 0; fd < NOFILE; fd++)
				{
					if (p->ofile[fd] != 0)
					{
						strFd = itoa(fd);
						dirent.inum = (procIndex + 1)*1000 +fd;
						safestrcpy(dirent.name, strFd, strlen(strFd) + 1);
						memmove(buf + offset, (void*)&dirent, sizeof(dirent));
						offset += sizeof(dirent);
					}
				}
			break;
		case STATUS:
				cprintf("Proc state: %s, Proc size: %d\n", procstateNames[p->state], p->sz);
			break;
	}

	release(&ptable.lock);
	return offset;
}

uint
printFdLevel(char *buf, uint inum)
{
	struct file *f;
	struct proc *p;
	int procIndex, fd;



	procIndex = ((int)(inum / 1000)) - 1;
	//fd can be up to NOFILE, we dont want mod10
	fd = inum % 100;

	acquire(&ptable.lock);
	p = &ptable.proc[procIndex];

	f = p->ofile[fd];
	cprintf("fd: %d, Type: %s, Offset: %d, Flags: R = %d, W = %d\n", fd, fileType[f->type], f->off, f->readable, f->writable);

	release(&ptable.lock);
	return 0;
}

int
procfsread(struct inode *ip, char *dst, int off, int n)
{
	char buf[NPROC* sizeof(struct dirent)];
	uint size;
	int remainder;

	switch(ip->minor)
	{
		case PROCFS_DIR:
			size = printProcLevel(buf);
			break;
		case PROCFS_PID:
			size = printPidLevel(buf, ip->inum);
			break;
		case PROCFS_FILE:
			size = printFileLevel(buf, ip->inum);
			break;
		case PROCFS_FD:
			size = printFdLevel(buf, ip->inum);
			break;
	}

	if (off < size)
	{
		remainder = (size - off);
		if (remainder > n)
			remainder = n;

		memmove(dst, buf + off, remainder);
		return remainder;
	}

	return 0;
}

int
procfswrite(struct inode *ip, char *buf, int n)
{
  return 0;
}

void
procfsinit(void)
{
  devsw[PROCFS].isdir = procfsisdir;
  devsw[PROCFS].iread = procfsiread;
  devsw[PROCFS].write = procfswrite;
  devsw[PROCFS].read = procfsread;
}
